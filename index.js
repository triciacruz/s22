// console.log("hello")

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

//Register
/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.
*/
    
	 function register(username){

        let userExist = registeredUsers.includes(username);

        if(userExist){
            return (alert("Registration Failed. Username already exists!"));
        } else {
            registeredUsers.push(username);
            return (alert("Thank you for registering!"));
        }
    }

// Add friend to friendlist
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
*/

	function addFriend(username){
    	        
            // let foundUser = registeredUsers.find(function(user){

            //     return user === username;
            // });

    
        let foundUser= registeredUsers.includes(username);

        if(foundUser){
            friendsList.push(username);
            return (alert("You have added "+ username + " as a friend!"));
        } else {
            return (alert("User not found."));
        }
    }

// // Diplay friends - list names

// /*
//     3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
//         - If the friendsList is empty show an alert: 
//             - "You currently have 0 friends. Add one first."
//         - Invoke the function.
// */

	function displayFriends(){


        if(friendsList.length > 0){

            friendsList.forEach(function(friend){
                console.log(friend);
            })

        } else {
           return (alert("You have " + friendsList.length + " friends. Add one first."));
        }
    };
    

// // Display number of friends
// /*
//     4. Create a function which will display the amount of registered users in your friendsList.
//         - If the friendsList is empty show an alert:
//             - "You currently have 0 friends. Add one first."
//         - If the friendsList is not empty show an alert:
//             - "You currently have <numberOfFriends> friends."
//         - Invoke the function

// */

	 function displayNumberOfFriends(){

        if(friendsList.length > 0){

            return (alert("You currently have " + friendsList.length + " friends."));


        } else {
            return (alert("You have " + friendsList.length + " friends. Add one first."));
        }
    };

// // Delete last element
// /*
//     5. Create a function which will delete the last registeredUser you have added in the friendsList.
//         - If the friendsList is empty show an alert:
//             - "You currently have 0 friends. Add one first."
//         - Invoke the function.
//         - Outside the function log the friendsList array.

// */
	
	function deleteFriend(){

        if(friendsList.length > 0){

            friendsList.pop();

        } else {
            return "You have " + friendsList.length + " friends. Add one first."
        }

    };

// /*
//     Stretch Goal:

//     Instead of only deleting the last registered user in the friendsList delete a specific user instead.
//         -You may get the user's index.
//         -Then delete the specific user with splice().

// */

	function deleteSpecificFriend(username){


    if(friendsList.length > 0){

        let userIndex = friendsList.indexOf(username);
        //if indexOf() cannot find the item or cannot find the index of the item, it will return -1
        friendsList.splice(userIndex,1);

    } else {
        return (alert("You have " + friendsList.length + " friends. Add one first."));
    }

	};







